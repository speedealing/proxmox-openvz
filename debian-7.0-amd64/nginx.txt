server {
listen 80;
#server_name nodeserver.inodbox.com;

root /var/www/speedealing;

#resolver 127.0.0.2;

rewrite ^/(.*)/favicon.ico$ /favicon.ico last;
rewrite ^/(.*)/logo.png$ /img/login-title.png last;

location ~ ^/(images/|img/|javascript/|js/|css/|stylesheets/|flash/|media/|static/|robots.txt|humans.txt|favicon.ico|favicon.png|includes/|fonts/) {
		root /var/www/speedealing/public;
		access_log off;
		expires 0;
		add_header Cache-Control private;
	}

location /errors {
		internal;
		alias /var/www/speedealing/public/errors;
	}

location / {
		set_real_ip_from 127.0.0.1;
		client_max_body_size 32M;
		client_body_buffer_size 256k;
		proxy_redirect off;
		proxy_read_timeout 300;
		proxy_set_header   X-Real-IP			$remote_addr;
		proxy_set_header   X-Forwarded-For		$proxy_add_x_forwarded_for;
		proxy_set_header   X-Forwarded-Proto	$scheme;
		proxy_set_header   Host					$http_host;
		proxy_set_header   X-NginX-Proxy		true;
		proxy_pass         http://localhost:3000;

		# supposedly prevents 502 bad gateway error;
		# ultimately not necessary in my case
		proxy_buffers 8 32k;
		proxy_buffer_size 64k;

		# the following is required as well for WebSockets
		proxy_http_version 1.1;
		proxy_set_header Upgrade $http_upgrade;
		proxy_set_header Connection "upgrade";
	}

}
