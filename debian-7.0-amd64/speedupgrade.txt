#!/bin/sh

APPDIR=$1
BRANCH=$2

die () {
    echo >&2 "$@"
    exit 1
}

info () {
    echo "speedupgrade.sh directory gitbranch"
}

if [ -z "$1" ]; then
    APPDIR=speedealing
fi

if [ -z "$2" ]; then
    BRANCH=node
fi

ROOTDIR=/var/www

if [ ! -d "$ROOTDIR" ]; then
    mkdir ${ROOTDIR}
fi

DOMAINDIR=${ROOTDIR}/${APPDIR}

if [ ! -d "$DOMAINDIR" ]; then
    mkdir ${DOMAINDIR}
fi

# Update git branch
cd /usr/local/speedealing
git checkout ${BRANCH}
git pull

if [ -z "$3" ]; then
    service nodeserver stop --force
fi

# Speedealing files update
cd $DOMAINDIR
find . ! -path "*/public/js/lib/*" ! -path "*/node_modules/*" -delete

cp -Rf /usr/local/speedealing/nodejs/* ${DOMAINDIR}
cp -f /usr/local/speedealing/nodejs/.bowerrc ${DOMAINDIR}
cp -f /root/development.json ${DOMAINDIR}/config/env
rm -f ${DOMAINDIR}/app/routes/migrate.js

find ${DOMAINDIR} -type f -exec chmod 644 \{\} \;
find ${DOMAINDIR} -type d -exec chmod 755 \{\} \;
#chown -R www-data:www-data ${DOMAINDIR}/public

if [ -z "$3" ]; then
	service nodeserver start
fi