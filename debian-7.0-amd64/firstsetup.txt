#!/bin/bash
### BEGIN INIT INFO
# Provides: speedealing
# Required-Start: $local_fs $network
# Required-Stop: $local_fs $remote_fs
# Should-Start: $all
# Should-Stop: $all
# Default-Start: 2 3 4 5
# Default-Stop: 0 1 6
# Short-Description: Speedealing first setup
### END INIT INFO

set -e

HOSTNAME=`head -n 1 /etc/hostname|awk '{ print $1; }'`

if [ "X${HOSTNAME}" = "Xlocalhost" ] ; then
        exit 0;
fi

# Generate a random password
#  $1 = number of characters; defaults to 32
#  $2 = include special characters; 1 = yes, 0 = no; defaults to 1
function randpass() {
  [ "$2" == "0" ] && CHAR="[:alnum:]" || CHAR="[:graph:]"
    cat /dev/urandom | tr -cd "$CHAR" | head -c ${1:-32}
    echo
}

# create random password
PASSWORD=`randpass 12 0`

# Set cookies password
sed -e 's/^\t*\"secret\"\:\s*\"1234qwerty\"/\t\t"secret": "'${PASSWORD}'"/' -i /root/development.json

update-rc.d firstsetup remove

rm -f $0